.code16 
.global _start                  # makes our label "init" available to the outside 

_start:                         # this is the beginning of our binary later. 
    welcome : .asciz "Hello World\n"
    jmp _boot
    .macro stampa_stringa_m str
    leaw \str, %si              #load effective address word di stringa into %si
    call .stampa_stringa
    .stampa_stringa:
        lodsb                   # carica un byte da %si dentro %al
        orb %al,%al             # or bit a bit, se torna zero ho incontrato la fine
        jz .stampa_stringa_out  # finisco
        movb $0x0e,%ah
        int $0x10
        jmp .stampa_stringa
    .stampa_stringa_out:
        ret
    
    .endm

_boot:
    stampa_stringa_m welcome
    .fill 510-(.-_start), 1, 0 # add zeroes to make it 510 bytes long 
    .word 0xaa55 # magic bytes that tell BIOS that this is bootable
